from django.urls import path
from receipts.views import recipts_list, create_receipt, expense_list, account_list, create_expense, create_account

urlpatterns = [
    path("", recipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expense_list, name="category_list"),
    path("categories/create/", create_expense, name="create_category"),
    path("accounts/", account_list, name="account_list" ),
    path("accounts/create/", create_account, name="create_account"),
]