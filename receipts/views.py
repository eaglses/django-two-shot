from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import ExpenseCategory, Account, Receipt
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
@login_required(redirect_field_name="/accounts/login/")
def recipts_list(request):
    recipts = Receipt.objects.filter(purchaser=request.user)
    #recipts = Receipt.objects.all()
    context = {"receipts_objects": recipts}
    return render(request, "receipts/list.html", context)

#expenses list
@login_required(redirect_field_name="/accounts/login/")
def expense_list(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expenses_objects": expenses}
    return render(request, "receipts/expenselist.html", context)

#accounts list
@login_required(redirect_field_name="/accounts/login/")
def account_list(request):
    expenses = Account.objects.filter(owner=request.user)
    context = {"accounts_objects": expenses}
    return render(request, "receipts/accountlist.html", context)

#create receipts 
@login_required(redirect_field_name="/accounts/login/")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt_inst = form.save(False)

            receipt_inst.purchaser  = request.user
            receipt_inst.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form, }
    return render(request, "receipts/create.html", context)


# expense create item
@login_required(redirect_field_name="/accounts/login/")
def create_expense(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt_inst = form.save(False)

            receipt_inst.owner  = request.user
            receipt_inst.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form, }
    return render(request, "receipts/create.html", context)


#account creat iteme
@login_required(redirect_field_name="/accounts/login/")
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt_inst = form.save(False)

            receipt_inst.owner  = request.user
            receipt_inst.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form, }
    return render(request, "receipts/create.html", context)
    